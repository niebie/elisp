;; This script is used to automatically create header guards for the current buffer.

(defun guard-header()
  ;; We ignore the capital H for header files, so ugly.
  (interactive)
  (if (string= (file-name-extension (buffer-name)) "h")
      (progn                            ;if
        (insert (concat "#ifndef _" (file-name-sans-extension (buffer-name)) "_H_\n"))
        (insert (concat "#define _" (file-name-sans-extension (buffer-name)) "_H_\n"))
        (insert "\n")
        (insert "#endif")
        )                               ;progn
    nil                                 ;else
    )                                   ;if
  )                                     ;guard-header

