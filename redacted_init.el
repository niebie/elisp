(package-initialize)

;; todo > use-package support?

;; Automatically enter web-mode for php files.
(add-to-list 'auto-mode-alist' ("\\.php" . web-mode))

;; Setup standard package repositories.
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.org/packages/")))

;; Several customizations done automatically by customize-variable.
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(before-save-hook nil)
 '(column-number-mode t)
 '(custom-enabled-themes (quote (base16-classic-dark)))
 '(custom-safe-themes
   (quote
    ("36282815a2eaab9ba67d7653cf23b1a4e230e4907c7f110eebf3cdf1445d8370" "dfebc0c405999cb1f69ec6a8dbad0bdc06d16382657021da2e189b4481d6d15d" default)))
 '(fci-rule-color "deep sky blue")
 '(js-indent-level 2)
 '(package-selected-packages
   (quote
    (fiplr uuidgen god-mode key-chord emacsql-psql linum-relative fill-column-indicator neotree helm afternoon-theme plantuml-mode csharp-mode json-mode web-mode geben))))

;; Disable some trimming that I don't care for.
(menu-bar-mode -1)
(scroll-bar-mode 0)
(tool-bar-mode -1)

;; Turn on display of column numbers.
(define-globalized-minor-mode global-column-number-mode
  column-number-mode
  (lambda ()
    (column-number-mode 1)))
(global-column-number-mode)

;; Disable the annoying splash screen.
(setq inhibit-startup-screen t)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(font-lock-comment-delimiter-face ((t (:foreground "gray31"))))
 '(font-lock-comment-face ((t (:foreground "medium spring green"))))
 '(font-lock-string-face ((t (:foreground "yellow"))))
 '(font-lock-variable-name-face ((t (:foreground "deep pink"))))
 '(mode-line ((t (:background "#14151E" :box (:line-width 1 :color "#eaeaea") :family "Courier New")))))

(require 'guid)
(require 'mu4e)

(require 'key-chord)
(key-chord-mode 1)

(setq key-chord-two-keys-delay 0.05)

(key-chord-define-global "rg"     'ripgrep-regexp)
(key-chord-define-global "lf"     'load-file)
(key-chord-define-global "rs"     'replace-string)
(key-chord-define-global "st"     'todo-show)
(key-chord-define-global "gm"     'god-mode)

(setq-default indent-tabs-mode nil)
